#!/usr/bin/env python3

"""
Calculates ultradian rhythm times
"""

from datetime import datetime
from datetime import timedelta

start = input("Enter your waking time (HH:MM):\t")
format_str = "%H:%M"
given_time = datetime.strptime(start, format_str)

work = 90 # min
chill= 20 # min
r = 1

while given_time.hour < 22:
    given_time = given_time + timedelta(minutes=work)
    start = given_time
    start_fmt = start.strftime('%H:%M')
    end = start + timedelta(minutes=work)
    end_fmt = end.strftime('%H:%M')
    print(f"Round #{r}: {start_fmt}-{end_fmt}")
    r += 1
    given_time += timedelta(minutes=chill)


