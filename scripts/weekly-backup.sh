#!/bin/bash

cd ~/devel/backup

tar cfj ssh.tar.gzip2 ~/.ssh/
tar cfj thunderbird.tar.gzip2 ~/Library/Thunderbird/Profiles/

git add *
git commit -m "Biweekly backup"
git push origin


