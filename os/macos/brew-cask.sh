#!/usr/bin/env bash

DIR=$(dirname "$0")
cd "$DIR"

. ../../scripts/functions.sh

info "Intalling brews cask ..."

# brew install caskroom/cask/brew-cask
# brew tap caskroom/cask
# brew tap caskroom/versions
# brew tap caskroom/drivers

# fonts
brew tap caskroom/fonts
brew install --cask font-awesome
brew install --cask font-hack-nerd-font

# essential
brew install --cask spectacle
brew install --cask iterm2
brew install --cask alfred
brew install --cask flux
brew install --cask signal
brew install --cask raindropio

# browsers
brew install --cask firefox
brew install --cask brave-browser

# authentication and security
brew install --cask santa
brew install --cask blockblock

# utils
brew install --cask spotify
brew install --cask disk-inventory-x
brew install --cask vlc
brew install --cask xquartz
brew install --cask appcleaner
brew install --cask teamviewer
brew install --cask iglance
brew install --cask michaelvillar-timer
brew install --cask gpg-suite
brew install --cask thunderbird
brew install --cask macclean
brew install --cask basictex
brew install --cask cryptomator
brew install --cask anki

# virtualisiation
brew install --cask virtualbox
brew install --cask vagrant
brew install --cask docker


# IDEs and editors
brew install --cask visual-studio-code
